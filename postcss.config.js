const purgecss = require('@fullhuman/postcss-purgecss')({
    content: [
        './src/**/*.html',
    ],
    defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || []
})
module.exports = {
    // modules: true,
    plugins: [
        require('tailwindcss'),
        require('autoprefixer'),
        require('postcss-nesting'),
        ...(process.env.NODE_ENV === 'production'
            ? [purgecss]
            : [])
    ]
}